package com.cj.homestaymanager;

import java.io.ByteArrayOutputStream;

import com.parse.Parse;
import com.parse.ParseFile;
import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class UploadFamilyPhotoActivity extends ActionBarActivity {

	static final int REQUEST_IMAGE_CAPTURE = 1;
	
	ImageView familyUserImageView;
	byte[] profilePicture;
	
	TextView uploadPhotoQTextView;
	
	Button uploadPhotoButton;
	Button finishButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_family_photo);
		//Parse.initialize(this, "2aQ6dNiMtTKPzrCKxT08icDP0laebuljRZB9IeRK", "rLGzN5C6M3EyTIQYebCWp2L6nXbRfwV3TEesMckg");
		
		setTitle("UploadPhotoActivity");
		
		familyUserImageView = (ImageView) findViewById(R.id.familyUserImageUploadView);
		
		uploadPhotoQTextView = (TextView) findViewById(R.id.uploadAPhotoQTextView);
		
		uploadPhotoButton = (Button) findViewById(R.id.uploadAPhotoButton);
		uploadPhotoButton.setOnClickListener((android.view.View.OnClickListener) uploadPhotoButtonListener);

		finishButton = (Button) findViewById(R.id.finishButton);
		finishButton.setOnClickListener((android.view.View.OnClickListener) finishButtonListener);

		
	}
	
	public OnClickListener uploadPhotoButtonListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			dispatchTakePictureIntent();
		}
	};
	
	public OnClickListener finishButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			ParseUser currentFamilyUser = ParseUser.getCurrentUser();
			ParseFile profPicFile = new ParseFile("profilePicture.png", profilePicture);
			profPicFile.saveInBackground();
			
			if (currentFamilyUser != null) {
				currentFamilyUser.put("profilePicture", profPicFile);
				currentFamilyUser.saveInBackground();
				
				if (currentFamilyUser.getString("userType").equals("family")) {
					Intent intent = new Intent(UploadFamilyPhotoActivity.this, FamilyUserMenuActivity.class);
					startActivity(intent);
				}
				else {
					Intent intent = new Intent(UploadFamilyPhotoActivity.this, StudentUserMenuActivity.class);
					startActivity(intent);
				}
			}
			else {
				Context context = getApplicationContext();
				CharSequence text = "Failed to upload picture...";
				int duration = Toast.LENGTH_SHORT;
				
				Toast toast = Toast.makeText(context, text, duration);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				
				Log.d("UploadFamilyPhotoActivity", "Failed to upload picture");
			}
		}
	};
	
	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if(takePictureIntent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
			Bundle extras = data.getExtras();
			Bitmap imageBitmap = (Bitmap) extras.get("data");
			familyUserImageView.setImageBitmap(imageBitmap);
			
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
			profilePicture = stream.toByteArray();
			
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.upload_family_photo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
