package com.cj.homestaymanager;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Build;

public class ViewLimitedProfileActivity extends ActionBarActivity {

	ImageView profilePictureTextView;
	TextView nameTextView;
	String name;
	TextView addressTextView;
	String address;
	TextView availabilityTextView;
	String startDate;
	String endDate;
	TextView blurbTextView;
	String blurb;
	
	Button connectButton;
	
	ParseUser currentUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_limited_profile);
		
		nameTextView = (TextView) findViewById(R.id.viewLimitedProfileNameTextView);
		addressTextView = (TextView) findViewById(R.id.viewLimitedProfileAddressTextView);
		availabilityTextView = (TextView) findViewById(R.id.viewLimitedProfileAvailabilityTextView);
		blurbTextView = (TextView) findViewById(R.id.viewLimitedProfileBlurbTextView);
		
		connectButton = (Button) findViewById(R.id.viewLimitedProfileConnectButton);
		connectButton.setOnClickListener((android.view.View.OnClickListener) connectListener);
		
		Bundle extras = getIntent().getExtras();
		
		if(extras != null) {
			ParseQuery<ParseUser> query = ParseUser.getQuery();
			query.whereEqualTo("username", extras.getString("EXTRA_USERNAME"));
			query.findInBackground(new FindCallback<ParseUser>() {
				@Override
				public void done(List<ParseUser> users, ParseException e) {
					if(e == null) {
						currentUser = users.get(0);
						if(currentUser.getString("userType").equals("admin"))
							Log.d("ViewLimitedProfileActivity", "Can't view an admin profile");
						else {
							getProfilePicture(currentUser);
							name = currentUser.getString("familyName");
							address = currentUser.getString("address");
							startDate = currentUser.getString("startDate");
							endDate = currentUser.getString("endDate");
							blurb = currentUser.getString("personalStatement");
							Log.d("ViewLimitedProfileActivity", blurb);
							updateProfile();
						}
					}
					else {
						Log.d("ViewFamilyProfile", "Coudln't find the family D:");
					}
					
				}
				
			});
		}
	}

	public OnClickListener connectListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			ParseUser student = ParseUser.getCurrentUser();
			
			JSONArray connectingFamilies = new JSONArray();
			
			if(student.getJSONArray("connectingUsers") == null) {
				connectingFamilies.put(currentUser.getUsername());
				student.put("connectingFamilies", connectingFamilies);
			}
			else {
				connectingFamilies = student.getJSONArray("connectingFamilies");
				boolean hasFamilyAlready = false;
				
				for(int i = 0; i < connectingFamilies.length(); i++) {
					try {
						if(connectingFamilies.getString(i).equals(currentUser.getUsername()))
							hasFamilyAlready = true;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(!hasFamilyAlready) {
					connectingFamilies.put(currentUser.getUsername());
					student.put("connectingFamilies", connectingFamilies);
				}
				
			}
			
			student.saveInBackground();
			
		}

	};
	
	private void updateProfile() {
		//display name
		if(currentUser.getString("userType").equals("family"))
			nameTextView.setText("The " + currentUser.getString("familyName") + " Family");
		else
			nameTextView.setText(currentUser.getString("fullName"));
		
		addressTextView.setText(address);
		
		availabilityTextView.setText("available " + formatDate(startDate) + " - " + formatDate(endDate));
		
		blurbTextView.setText("\"" + blurb + "\"");
		
	}
	
	private String formatDate(String date) {
		if(date.length() == 8) {
			String buffer = "";
			buffer += date.substring(0,2);
			buffer += "/";
			buffer += date.substring(2,4);
			buffer += "/";
			buffer += date.substring(4, date.length());
			
			return buffer;
		}
		else
			return "Oops, wrong format!";
	}
	
	private void getProfilePicture(ParseUser currentFamily) {
		ParseFile picture = (ParseFile)currentFamily.get("profilePicture");
		picture.getDataInBackground(new GetDataCallback() {
			@Override
			public void done(byte[] data, ParseException e) {
				if(e == null) {
					//convert ByteArray to Bitmap
					ImageView profilePictureImageView = (ImageView) findViewById(R.id.viewLimitedProfilePictureImageView);
					profilePictureImageView.setImageBitmap(BitmapFactory.decodeByteArray(data, 0, data.length));
				}
				else {
					Log.d("ViewFamilyProfileActivity", "Couldn't get the picture");
				}	
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_limited_profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
