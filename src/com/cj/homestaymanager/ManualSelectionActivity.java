package com.cj.homestaymanager;

import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class ManualSelectionActivity extends ActionBarActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manual_selection);
		
		ArrayList<ParseUser> familyInfo = new ArrayList<ParseUser>();
		
		final FamilyArrayAdapter listAdapter = new FamilyArrayAdapter(this, familyInfo);
		
		final ListView familiesListView = (ListView) findViewById(R.id.manualSelectionFamiliesListView);
		familiesListView.setOnItemClickListener(familiesListViewItemListener);
		
		familiesListView.setAdapter(listAdapter);
		
		final ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo("userType", "family");
		query.findInBackground(new FindCallback<ParseUser>() {
			@Override
			public void done(List<ParseUser> families, ParseException e) {
				if(e == null) {
					for(ParseUser family : families) {
						listAdapter.add(family);
					}
				}
				else {
					Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	
	public OnItemClickListener familiesListViewItemListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
			
			LinearLayout linearLayout = (LinearLayout) v;
			
			TextView usernameTextView = (TextView) linearLayout.findViewById(R.id.familyRowUsernameTextView);
			
			Intent intent = new Intent(ManualSelectionActivity.this, ViewLimitedProfileActivity.class);
			intent.putExtra("EXTRA_USERNAME", usernameTextView.getText().toString());
			startActivity(intent);
			
		}	
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manual_selection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
