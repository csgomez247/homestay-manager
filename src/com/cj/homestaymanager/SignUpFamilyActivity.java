package com.cj.homestaymanager;

import java.io.ByteArrayOutputStream;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpFamilyActivity extends ActionBarActivity {

static final int REQUEST_IMAGE_CAPTURE = 1;
	
	ImageView familyUserImageView;
	Button uploadStudentUserImageButton;
	byte[] profilePicture;
	
	TextView usernameTextView;
	TextView passwordTextView;
	TextView emailTextView;
	TextView phoneTextView;
	TextView addressTextView;
	
	EditText usernameEditText;
	String username;
	
	EditText passwordEditText;
	String password;
	
	TextView familyNameTextView;
	String familyName;
	EditText familyNameEditText;
	
	EditText emailEditText;
	String email;
	
	EditText phoneEditText;
	String phone;
	
	EditText addressEditText;
	String address;
	
	TextView startDateTextView;
	EditText startDateEditText;
	String startDate;
	
	TextView endDateTextView;
	EditText endDateEditText;
	String endDate;
	
	RadioButton smoking;
	RadioButton nonSmoking;
	Boolean allowsSmoking;
	
	TextView notComfortableWithTextView;
	
	CheckBox dogsCheckBox;
	Boolean noDogs;
	
	CheckBox catsCheckBox;
	Boolean noCats;
	
	CheckBox fishCheckBox;
	Boolean noFish;
	
	CheckBox birdsCheckBox;
	Boolean noBirds;
	
	CheckBox rabbitsCheckBox;
	Boolean noRabbits;
	
	CheckBox horsesCheckBox;
	Boolean noHorses;
	
	TextView otherAnimalsTextView;
	EditText otherAnimalsEditText;
	String otherAnimals;
	
	EditText numberOfKidsEditText;
	int numberOfKids;
	EditText numberOfAdultsEditText;
	int numberOfAdults;
	TextView kidsTextView;
	TextView adultsTextView;
	CheckBox maleKidsCheckBox;
	boolean hasMaleKids;
	CheckBox maleAdultsCheckBox;
	boolean hasMaleAdults;
	CheckBox femaleKidsCheckBox;
	boolean hasFemaleKids;
	CheckBox femaleAdultsCheckBox;
	boolean hasFemaleAdults;
	CheckBox otherKidsCheckBox;
	boolean hasOtherKids;
	CheckBox otherAdultsCheckBox;
	boolean hasOtherAdults;
	
	TextView dietTextView;
	CheckBox peanutsCheckBox;
	CheckBox treeNutsCheckBox;
	CheckBox milkCheckBox;
	CheckBox eggsCheckBox;
	CheckBox soyCheckBox;
	CheckBox wheatCheckBox;
	CheckBox fishFoodCheckBox;
	CheckBox shellFishCheckBox;
	boolean eatsPeanuts;
	boolean eatsTreeNuts;
	boolean eatsMilk;
	boolean eatsEggs;
	boolean eatsSoy;
	boolean eatsWheat;
	boolean eatsFish;
	boolean eatsShellfish;
	
	TextView genderPreferenceTextView;
	
	CheckBox malePreferenceCheckBox;
	Boolean preferMale;
	CheckBox femalePreferenceCheckBox;
	Boolean preferFemale;
	CheckBox otherPreferenceCheckBox;
	Boolean preferOther;
	CheckBox noPreferenceCheckBox;
	Boolean noGenderPreference;
	
	TextView personalStatementTextView;
	EditText personalStatementEditText;
	String personalStatement;
	
	Button signUpStudentButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up_family);
		//Parse.initialize(this, "2aQ6dNiMtTKPzrCKxT08icDP0laebuljRZB9IeRK", "rLGzN5C6M3EyTIQYebCWp2L6nXbRfwV3TEesMckg");
		
		usernameTextView = (TextView) findViewById(R.id.usernameLogInTextView);
		usernameEditText = (EditText) findViewById(R.id.usernameEditText);
		
		passwordTextView = (TextView) findViewById(R.id.passwordTextView);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		
		familyNameTextView = (TextView) findViewById(R.id.viewFamilyProfileFamilyNameTextView);
		familyNameEditText = (EditText) findViewById(R.id.familyNameEditText);
		
		emailTextView = (TextView) findViewById(R.id.emailTextView);
		emailEditText = (EditText) findViewById(R.id.emailEditText);
				
		phoneTextView = (TextView) findViewById(R.id.phoneTextView);
		phoneEditText = (EditText) findViewById(R.id.phoneEditText);
				
		addressTextView = (TextView) findViewById(R.id.homeAddressTextView);
		addressEditText = (EditText) findViewById(R.id.addressEditText);
		
		startDateTextView = (TextView) findViewById(R.id.signUpFamilyStartDateTextView);
		startDateEditText = (EditText) findViewById(R.id.signUpFamilyStartDateEditText);
		
		endDateTextView = (TextView) findViewById(R.id.signUpFamilyEndDateTextView);
		endDateEditText = (EditText) findViewById(R.id.signUpFamilyEndDateEditText);
		
		smoking = (RadioButton) findViewById(R.id.smokingRadio);
		nonSmoking = (RadioButton) findViewById(R.id.nonSmokingRadio);
		
		notComfortableWithTextView = (TextView) findViewById(R.id.notComfortableWithTextView);
		
		dogsCheckBox = (CheckBox) findViewById(R.id.dogsCheckBox);
		catsCheckBox = (CheckBox) findViewById(R.id.catsCheckBox);
		fishCheckBox = (CheckBox) findViewById(R.id.fishCheckBox);
		birdsCheckBox = (CheckBox) findViewById(R.id.birdsCheckBox);
		rabbitsCheckBox = (CheckBox) findViewById(R.id.rabbitsCheckBox);
		horsesCheckBox = (CheckBox) findViewById(R.id.horsesCheckBox);
		
		otherAnimalsTextView = (TextView) findViewById(R.id.otherAnimalsTextView);
		otherAnimalsEditText = (EditText) findViewById(R.id.otherAnimalsEditText);
		
		numberOfKidsEditText = (EditText) findViewById(R.id.signUpFamilyNumberOfKidsEditText);
		numberOfAdultsEditText = (EditText) findViewById(R.id.signUpFamilyNumberOfAdultsEditText);
		kidsTextView = (TextView) findViewById(R.id.signUpFamilyKidsTextView);
		adultsTextView = (TextView) findViewById(R.id.signUpFamilyAdultsTextView);
		maleKidsCheckBox = (CheckBox) findViewById(R.id.signUpFamilyMaleKidsCheckBox);
		maleAdultsCheckBox = (CheckBox) findViewById(R.id.signUpFamilyMaleAdultCheckBox);
		femaleKidsCheckBox = (CheckBox) findViewById(R.id.signUpFamilyFemaleKidsCheckBox);
		femaleAdultsCheckBox = (CheckBox) findViewById(R.id.signUpFamilyFemaleAdultCheckBox);
		otherKidsCheckBox = (CheckBox) findViewById(R.id.signUpFamilyOtherKidsCheckBox);
		otherAdultsCheckBox = (CheckBox) findViewById(R.id.signUpFamilyOtherAdultCheckBox);
		
		dietTextView = (TextView) findViewById(R.id.signUpFamilyDietTextView);
		peanutsCheckBox = (CheckBox) findViewById(R.id.signUpFamilyPeanutsCheckBox);
		treeNutsCheckBox = (CheckBox) findViewById(R.id.signUpFamilyTreeNutsCheckBox);
		milkCheckBox = (CheckBox) findViewById(R.id.signUpFamilyMilkCheckBox);
		eggsCheckBox = (CheckBox) findViewById(R.id.signUpFamilyEggsCheckBox);
		soyCheckBox = (CheckBox) findViewById(R.id.signUpFamilySoyCheckBox);
		wheatCheckBox = (CheckBox) findViewById(R.id.signUpFamilyWheatCheckBox);
		fishFoodCheckBox = (CheckBox) findViewById(R.id.signUpFamilyFishFoodCheckBox);
		shellFishCheckBox = (CheckBox) findViewById(R.id.signUpFamilyShelfishCheckBox);
		
		genderPreferenceTextView = (TextView) findViewById(R.id.genderPreferenceTextView);
		
		malePreferenceCheckBox = (CheckBox) findViewById(R.id.malePreferenceCheckBox);
		femalePreferenceCheckBox = (CheckBox) findViewById(R.id.femalePreferenceCheckBox);
		otherPreferenceCheckBox = (CheckBox) findViewById(R.id.otherPreferenceCheckBox);
		noPreferenceCheckBox = (CheckBox) findViewById(R.id.noPreferenceCheckBox);
		
		signUpStudentButton = (Button) findViewById(R.id.signUpStudentButton);
		signUpStudentButton.setOnClickListener((android.view.View.OnClickListener) signUpStudentButtonListener);
		
		personalStatementTextView = (TextView) findViewById(R.id.signUpFamilyPersonalStatementTextView);
		personalStatementEditText = (EditText) findViewById(R.id.signUpFamilyPersonalStatementEditText);
				
	}
	
	public OnClickListener signUpStudentButtonListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			
			Boolean allFieldsValid = true;
			
			//username
			if(!usernameEditText.getText().toString().equals("")) {
				username = usernameEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpFamilyActivity", "No username entered");
			}
			
			//password
			if(!passwordEditText.getText().toString().equals("")) {
				password = passwordEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpFamilyActivity", "No password entered");
			}
			
			//family name
			if(!familyNameEditText.getText().toString().equals("")) {
				familyName = familyNameEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpFamilyAcitivity", "No family name entered");
			}
			
			//email
			if(!emailEditText.getText().toString().equals("")) {
				email = emailEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpFamilyActivity", "No email entered");
			}
			
			//phone
			if(!phoneEditText.getText().toString().equals("")) {
				phone = phoneEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpFamilyActivity", "No phone entered");
			}
			
			//address
			if(!addressEditText.getText().toString().equals("")) {
				address = addressEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpFamilyActivity", "No address entered");
			}
			
			//start date
			if(!startDateEditText.getText().toString().equals("")) {
				startDate = startDateEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpFamilyActivity", "No start date entered");
			}
			
			//end date
			if(!endDateEditText.getText().toString().equals("")) {
				endDate = endDateEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpFamilyActivity", "No end date entered");
			}
			
			//smoking or non-smoking
			if(nonSmoking.isChecked()) {
				allowsSmoking = false;
			}
			else {
				allowsSmoking = true;
			}
			
			if(dogsCheckBox.isChecked()) {
				noDogs = true;
			}
			else {
				noDogs = false;
			}
			
			if(catsCheckBox.isChecked()) {
				noCats = true;
			}
			else {
				noCats = false;
			}
			
			if(fishCheckBox.isChecked()) {
				noFish = true;
			}
			else {
				noFish = false;
			}
			
			if(birdsCheckBox.isChecked()) {
				noBirds = true;
			}
			else {
				noBirds = false;
			}
			
			if(rabbitsCheckBox.isChecked()) {
				noRabbits = true;
			}
			else {
				noRabbits = false;
			}
			
			if(horsesCheckBox.isChecked()) {
				noHorses = true;
			}
			else {
				noHorses = false;
			}
			
			/*
			 * family specifics
			 */
			if(!numberOfKidsEditText.getText().toString().equals(""))
				numberOfKids = Integer.parseInt(numberOfKidsEditText.getText().toString());
			else
				allFieldsValid = false;
			
			if(!numberOfAdultsEditText.getText().toString().equals(""))
				numberOfAdults = Integer.parseInt(numberOfAdultsEditText.getText().toString());
			else
				allFieldsValid = false;
			
			if(maleKidsCheckBox.isChecked())
				hasMaleKids = true;
			else
				hasMaleKids = false;
			
			if(femaleKidsCheckBox.isChecked())
				hasFemaleKids = true;
			else
				hasFemaleKids = false;
			
			if(otherKidsCheckBox.isChecked())
				hasOtherKids = true;
			else
				hasOtherKids = false;
			
			if(maleAdultsCheckBox.isChecked())
				hasMaleAdults = true;
			else
				hasMaleAdults = false;
			
			if(femaleAdultsCheckBox.isChecked())
				hasFemaleAdults = true;
			else
				hasFemaleAdults = false;
			
			if(otherAdultsCheckBox.isChecked())
				hasOtherAdults = true;
			else
				hasOtherAdults = false;
			
			/*
			 * diet specifics
			 */
			if(peanutsCheckBox.isChecked())
				eatsPeanuts = true;
			else
				eatsPeanuts = false;

			if(treeNutsCheckBox.isChecked())
				eatsTreeNuts = true;
			else
				eatsTreeNuts = false;
			
			if(milkCheckBox.isChecked())
				eatsMilk = true;
			else
				eatsMilk = false;
			
			if(eggsCheckBox.isChecked())
				eatsEggs = true;
			else
				eatsEggs = false;
			
			if(wheatCheckBox.isChecked())
				eatsWheat = true;
			else
				eatsWheat = false;
			
			if(soyCheckBox.isChecked())
				eatsSoy = true;
			else
				eatsSoy = false;
			
			if(fishFoodCheckBox.isChecked())
				eatsFish = true;
			else
				eatsFish = false;
			
			if(shellFishCheckBox.isChecked())
				eatsShellfish = true;
			else
				eatsShellfish = false;
			
			/*
			 * gender preferences
			 */
			if(malePreferenceCheckBox.isChecked()) {
				preferMale = true;
			}
			else {
				preferMale = false;
			}
			
			if(femalePreferenceCheckBox.isChecked()) {
				preferFemale = true;
			}
			else {
				preferFemale = false;
			}
			
			if(otherPreferenceCheckBox.isChecked()) {
				preferOther = true;
			}
			else {
				preferOther = false;
			}
			
			if(noPreferenceCheckBox.isChecked()) {
				noGenderPreference = true;
			}
			else {
				noGenderPreference = false;
			}
			
			if(!personalStatementEditText.getText().toString().equals("")) {
				personalStatement = personalStatementEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
			}
			
			//other animals
			otherAnimals = otherAnimalsEditText.getText().toString();
			
			if(allFieldsValid) {
				//push information to the database
				ParseUser family = new ParseUser();
				
				family.setUsername(username);
				family.setPassword(password);
				family.setEmail(email);
				
				family.put("userType", "family");
				family.put("familyName", familyName);
				family.put("phone", phone);
				family.put("address", address);
				family.put("startDate", startDate);
				family.put("endDate", endDate);
				family.put("allowsSmoking", allowsSmoking);
				family.put("hasDogs", noDogs);
				family.put("hasCats", noCats);
				family.put("hasFish", noFish);
				family.put("hasBirds", noBirds);
				family.put("hasRabbits", noRabbits);
				family.put("hasHorses", noHorses);
				family.put("otherAnimals", otherAnimals);
				family.put("hasMaleKids", hasMaleKids);
				family.put("hasFemaleKids", hasFemaleKids);
				family.put("hasOtherKids", hasOtherKids);
				family.put("hasMaleAdults", hasMaleAdults);
				family.put("hasFemaleAdults", hasFemaleAdults);
				family.put("hasOtherAdults", hasOtherAdults);
				family.put("numKidsInFamily", numberOfKids);
				family.put("numAdultsInFamily", numberOfAdults);
				family.put("eatsPeanuts", eatsPeanuts);
				family.put("eatsTreeNuts", eatsTreeNuts);
				family.put("eatsMilk", eatsMilk);
				family.put("eatsEggs", eatsEggs);
				family.put("eatsWheat", eatsWheat);
				family.put("eatsSoy", eatsSoy);
				family.put("eatsFish", eatsFish);
				family.put("eatsShellfish", eatsShellfish);
				family.put("preferMale", preferMale);
				family.put("preferFemale", preferFemale);
				family.put("preferOther", preferOther);
				family.put("noGenderPreference", noGenderPreference);
				family.put("personalStatement", personalStatement);
				
				family.signUpInBackground(new SignUpCallback() {
					@Override
					public void done(ParseException e) {
						if(e == null) {
							Intent intent = new Intent(SignUpFamilyActivity.this, UploadFamilyPhotoActivity.class);
							startActivity(intent);
						}
						else {
							Context context = getApplicationContext();
							CharSequence text = e.getMessage();
							int duration = Toast.LENGTH_SHORT;
							
							Toast toast = Toast.makeText(context, text, duration);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							
							Log.d("SignUpFamilyActivity", e.getMessage());
						}
					}
				});
			}
			else {
				Context context = getApplicationContext();
				CharSequence text = "Please fill in all text fields";
				int duration = Toast.LENGTH_SHORT;
				
				Toast toast = Toast.makeText(context, text, duration);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_up_family, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
