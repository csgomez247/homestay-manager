package com.cj.homestaymanager;

import com.parse.Parse;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SignUpActivity extends ActionBarActivity {

	TextView whoAreYouTextView;
	
	Button studentButton;
	Button familyButton;
	Button adminButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
        //Parse.initialize(this, "2aQ6dNiMtTKPzrCKxT08icDP0laebuljRZB9IeRK", "rLGzN5C6M3EyTIQYebCWp2L6nXbRfwV3TEesMckg");
        
        whoAreYouTextView = (TextView) findViewById(R.id.whoAreYouTextView);
        
        studentButton = (Button) findViewById(R.id.studentButton);
        studentButton.setOnClickListener((android.view.View.OnClickListener) studentButtonListener);
        
        familyButton = (Button) findViewById(R.id.familyButton);
        familyButton.setOnClickListener((android.view.View.OnClickListener) familyButtonListener);
        
        adminButton = (Button) findViewById(R.id.adminButton);
        adminButton.setOnClickListener((android.view.View.OnClickListener) adminButtonListener);
	}
	
	public OnClickListener studentButtonListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent(SignUpActivity.this, SignUpStudentActivity.class);
			startActivity(intent);
		}
	};
	
	public OnClickListener familyButtonListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent(SignUpActivity.this, SignUpFamilyActivity.class);
			startActivity(intent);
		}
	};
	
	public OnClickListener adminButtonListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent(SignUpActivity.this, SignUpAdminActivity.class);
			startActivity(intent);
		}
	};
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_up, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
