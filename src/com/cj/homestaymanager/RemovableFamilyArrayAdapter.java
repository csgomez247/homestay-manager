package com.cj.homestaymanager;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.parse.ParseUser;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RemovableFamilyArrayAdapter extends ArrayAdapter<ParseUser> {
	
	private final Context context;
	private final ArrayList<ParseUser> familiesArrayList;
	
	public RemovableFamilyArrayAdapter(Context context, ArrayList<ParseUser> familiesArrayList) {
		super(context, R.layout.removable_family_row, familiesArrayList);
		
		this.context = context;
		this.familiesArrayList = familiesArrayList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View familyRowView = inflater.inflate(R.layout.removable_family_row, parent, false);
		
		TextView familyNameTextView = (TextView) familyRowView.findViewById(R.id.removableFamilyRowFamilyNameTextView);
		TextView usernameTextView = (TextView) familyRowView.findViewById(R.id.removableFamilyRowUsernameTextView);
		TextView locationTextView = (TextView) familyRowView.findViewById(R.id.removableFamilyRowLocationTextView);
		TextView availabilityTextView = (TextView) familyRowView.findViewById(R.id.removableFamilyRowAvailabilityTextView);
		TextView personalStatementTextView = (TextView) familyRowView.findViewById(R.id.removableFamilyRowPersonalStatementTextView);
		
		Button deleteButton = (Button) familyRowView.findViewById(R.id.removableFamilyRowDeleteButton);
		deleteButton.setOnClickListener((android.view.View.OnClickListener) deleteButtonListener);
		
		familyNameTextView.setText(familiesArrayList.get(position).getString("familyName"));
		usernameTextView.setText(familiesArrayList.get(position).getUsername());
		locationTextView.setText(familiesArrayList.get(position).getString("address"));
		availabilityTextView.setText(formatDate(familiesArrayList.get(position).getString("startDate")) + " - " + 
				formatDate(familiesArrayList.get(position).getString("endDate")));
		personalStatementTextView.setText("\"" + familiesArrayList.get(position).getString("personalStatement") + "\"");
		
		return familyRowView;
		
	}
	
	public OnClickListener deleteButtonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			
			LinearLayout linearLayout = (LinearLayout) v.getParent().getParent();
			
			TextView familyUsernameTextView = (TextView) linearLayout.findViewById(R.id.removableFamilyRowUsernameTextView);
			
			String familyUsername = familyUsernameTextView.getText().toString();
			
			ParseUser student = ParseUser.getCurrentUser();
			
			JSONArray savedFamilies = new JSONArray();
			JSONArray newSavedFamilies = new JSONArray();
			
			if(student.getJSONArray("savedFamilies") == null) {
				Log.d("RemovableFamilyArrayAdapter", "hmm, trying to remove something that wasn't saved");
			}
			else {
				savedFamilies = student.getJSONArray("savedFamilies");
				
				for(int i = 0; i < savedFamilies.length(); i++) {
					try {
						if(!savedFamilies.getString(i).equals(familyUsername)) {
							newSavedFamilies.put(savedFamilies.get(i));
						}	
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				student.put("savedFamilies", newSavedFamilies);
				
			}
			student.saveInBackground();
		}
		
	};
	
	private String formatDate(String date) {
		if(date.length() == 8) {
			String buffer = "";
			buffer += date.substring(0,2);
			buffer += "/";
			buffer += date.substring(2,4);
			buffer += "/";
			buffer += date.substring(4, date.length());
			
			return buffer;
		}
		else
			return "Oops, wrong format!";
	}

}












