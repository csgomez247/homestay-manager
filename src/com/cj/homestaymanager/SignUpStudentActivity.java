package com.cj.homestaymanager;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpStudentActivity extends ActionBarActivity {
	
	static final int REQUEST_IMAGE_CAPTURE = 1;
	
	ImageView studentUserImageView;
	Button uploadStudentUserImageButton;
	byte[] profilePicture;
	
	TextView usernameTextView;
	TextView passwordTextView;
	TextView fullNameTextView;
	TextView emailTextView;
	TextView phoneTextView;
	TextView addressTextView;
	
	EditText usernameEditText;
	String username;
	
	EditText passwordEditText;
	String password;
	
	EditText fullNameEditText;
	String fullName;
	
	EditText emailEditText;
	String email;
	
	EditText phoneEditText;
	String phone;
	
	EditText addressEditText;
	String address;
	
	TextView studentIDTextView;
	TextView departureTextView;
	TextView arrivalTextView;
	
	EditText studentIDEditText;
	String studentID;
	
	EditText departureEditText;
	String departureDate;
	
	EditText arrivalEditText;
	String arrivalDate;
	
	TextView genderIDTextView;
	RadioButton maleRadio;
	boolean isMale;
	RadioButton femaleRadio;
	boolean isFemale;
	RadioButton otherRadio;
	boolean isOther;
	
	RadioButton smoking;
	RadioButton nonSmoking;
	Boolean allowsSmoking;
	
	TextView notComfortableWithTextView;
	
	CheckBox dogsCheckBox;
	Boolean noDogs;
	
	CheckBox catsCheckBox;
	Boolean noCats;
	
	CheckBox fishCheckBox;
	Boolean noFish;
	
	CheckBox birdsCheckBox;
	Boolean noBirds;
	
	CheckBox rabbitsCheckBox;
	Boolean noRabbits;
	
	CheckBox horsesCheckBox;
	Boolean noHorses;
	
	TextView otherAnimalsTextView;
	EditText otherAnimalsEditText;
	String otherAnimals;
	
	TextView allergicToTextView;
	
	CheckBox peanutsCheckBox;
	Boolean noPeanuts;
	
	CheckBox treeNutsCheckBox;
	Boolean noTreeNuts;
	
	CheckBox milkCheckBox;
	Boolean noMilk;
	
	CheckBox eggsCheckBox;
	Boolean noEggs;
	
	CheckBox wheatCheckBox;
	Boolean noWheat;
	
	CheckBox soyCheckBox;
	Boolean noSoy;
	
	CheckBox fishFoodCheckBox;
	Boolean noFishFood;
	
	CheckBox shellfishCheckBox;
	Boolean noShellfish;
	
	TextView otherAllergiesTextView;
	EditText otherAllergiesEditText;
	String otherAllergies;
	
	TextView comfortableWithTextView;
	
	TextView numKidsTextView;
	EditText numKidsEditText;
	int numKids;
	
	CheckBox maleKidCheckBox;
	Boolean maleKidOk;
	CheckBox femaleKidCheckBox;
	Boolean femaleKidOk;
	CheckBox otherKidCheckBox;
	Boolean otherKidOk;
	
	TextView numAdultsTextView;
	EditText numAdultsEditText;
	int numAdults;
	
	CheckBox maleAdultCheckBox;
	Boolean maleAdultOk;
	CheckBox femaleAdultCheckBox;
	Boolean femaleAdultOk;
	CheckBox otherAdultCheckBox;
	Boolean otherAdultOk;
	
	TextView personalStatementTextView;
	EditText personalStatementEditText;
	String personalStatement;
	
	Button signUpStudentButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up_student);
		//Parse.initialize(this, "2aQ6dNiMtTKPzrCKxT08icDP0laebuljRZB9IeRK", "rLGzN5C6M3EyTIQYebCWp2L6nXbRfwV3TEesMckg");
		
		usernameTextView = (TextView) findViewById(R.id.usernameLogInTextView);
		usernameEditText = (EditText) findViewById(R.id.usernameEditText);
		
		passwordTextView = (TextView) findViewById(R.id.passwordTextView);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		
		fullNameTextView = (TextView) findViewById(R.id.signUpStudentFullNameTextView);
		fullNameEditText = (EditText) findViewById(R.id.signUpStudentFullNameEditText);
				
		emailTextView = (TextView) findViewById(R.id.emailTextView);
		emailEditText = (EditText) findViewById(R.id.emailEditText);
				
		phoneTextView = (TextView) findViewById(R.id.phoneTextView);
		phoneEditText = (EditText) findViewById(R.id.phoneEditText);
				
		addressTextView = (TextView) findViewById(R.id.homeAddressTextView);
		addressEditText = (EditText) findViewById(R.id.addressEditText);
		
		studentIDTextView = (TextView) findViewById(R.id.studentIDTextView);
		departureTextView = (TextView) findViewById(R.id.departureTextView);
		arrivalTextView = (TextView) findViewById(R.id.arrivalTextView);
		
		studentIDEditText = (EditText) findViewById(R.id.studentIDEditText);
		departureEditText = (EditText) findViewById(R.id.departureEditText);
		arrivalEditText = (EditText) findViewById(R.id.arrivalEditText);
		
		personalStatementTextView = (TextView) findViewById(R.id.signUpStudentPersonalStatementTextView);
		personalStatementEditText = (EditText) findViewById(R.id.signUpStudentPersonalStatementEditText);
		
		genderIDTextView = (TextView) findViewById(R.id.signUpStudentGenderIDTextView);
		maleRadio = (RadioButton) findViewById(R.id.signUpStudentMaleRadio);
		femaleRadio = (RadioButton) findViewById(R.id.signUpStudentFemaleRadio);
		otherRadio = (RadioButton) findViewById(R.id.signUpStudentOtherRadio);
		
		smoking = (RadioButton) findViewById(R.id.smokingRadio);
		nonSmoking = (RadioButton) findViewById(R.id.nonSmokingRadio);
		
		notComfortableWithTextView = (TextView) findViewById(R.id.notComfortableWithTextView);
		
		dogsCheckBox = (CheckBox) findViewById(R.id.dogsCheckBox);
		catsCheckBox = (CheckBox) findViewById(R.id.catsCheckBox);
		fishCheckBox = (CheckBox) findViewById(R.id.fishCheckBox);
		birdsCheckBox = (CheckBox) findViewById(R.id.birdsCheckBox);
		rabbitsCheckBox = (CheckBox) findViewById(R.id.rabbitsCheckBox);
		horsesCheckBox = (CheckBox) findViewById(R.id.horsesCheckBox);
		
		otherAnimalsTextView = (TextView) findViewById(R.id.otherAnimalsTextView);
		otherAnimalsEditText = (EditText) findViewById(R.id.otherAnimalsEditText);
		
		allergicToTextView = (TextView) findViewById(R.id.allergicToTextView);
		
		peanutsCheckBox = (CheckBox) findViewById(R.id.peanutCheckBox);
		treeNutsCheckBox = (CheckBox) findViewById(R.id.treeNutsCheckBox);
		milkCheckBox = (CheckBox) findViewById(R.id.milkCheckBox);
		eggsCheckBox = (CheckBox) findViewById(R.id.eggCheckBox);
		wheatCheckBox = (CheckBox) findViewById(R.id.wheatCheckBox);
		soyCheckBox = (CheckBox) findViewById(R.id.soyCheckBox);
		fishFoodCheckBox = (CheckBox) findViewById(R.id.fishFoodCheckBox);
		shellfishCheckBox = (CheckBox) findViewById(R.id.shellFishCheckBox);
		
		otherAllergiesTextView = (TextView) findViewById(R.id.otherAllergiesTextView);
		otherAllergiesEditText = (EditText) findViewById(R.id.otherAllergiesEditText);
		
		comfortableWithTextView = (TextView) findViewById(R.id.comfortableWithTextView);
		
		numKidsTextView = (TextView) findViewById(R.id.numKidsTextView);
		numKidsEditText = (EditText) findViewById(R.id.numKidsEditText);
		
		maleKidCheckBox = (CheckBox) findViewById(R.id.maleKidCheckBox);
		femaleKidCheckBox = (CheckBox) findViewById(R.id.femaleKidCheckBox);
		otherKidCheckBox = (CheckBox) findViewById(R.id.otherKidCheckBox);
		
		numAdultsTextView = (TextView) findViewById(R.id.numAdultsTextView);
		numAdultsEditText = (EditText) findViewById(R.id.numAdultsEditText);
		
		maleAdultCheckBox = (CheckBox) findViewById(R.id.maleAdultCheckBox);
		femaleAdultCheckBox = (CheckBox) findViewById(R.id.femaleAdultCheckBox);
		otherAdultCheckBox = (CheckBox) findViewById(R.id.otherAdultCheckBox);
		
		signUpStudentButton = (Button) findViewById(R.id.signUpStudentButton);
		signUpStudentButton.setOnClickListener((android.view.View.OnClickListener) signUpStudentButtonListener);
				
	}
	
	public OnClickListener uploadStudentUserImageButtonListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			dispatchTakePictureIntent();
		}
	};
	
	public OnClickListener signUpStudentButtonListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			
			Boolean allFieldsValid = true;
			
			//username
			if(!usernameEditText.getText().toString().equals("")) {
				username = usernameEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpStudentActivity", "No username entered");
			}
			
			//password
			if(!passwordEditText.getText().toString().equals("")) {
				password = passwordEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpStudentActivity", "No password entered");
			}
			
			//fullName
			if(!fullNameEditText.getText().toString().equals("")) {
				fullName = fullNameEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpStudentActivity", "No fullName entered");
			}
			
			//email
			if(!emailEditText.getText().toString().equals("")) {
				email = emailEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpStudentActivity", "No email entered");
			}
			
			//phone
			if(!phoneEditText.getText().toString().equals("")) {
				phone = phoneEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpStudentActivity", "No phone entered");
			}
			
			//address
			if(!addressEditText.getText().toString().equals("")) {
				address = addressEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpStudentActivity", "No address entered");
			}
			
			//student id
			if(!studentIDEditText.getText().toString().equals("")) {
				studentID = studentIDEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpStudentActivity", "No studentID entered");
			}
			
			//departure date
			if(!departureEditText.getText().toString().equals("")) {
				departureDate = departureEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpStudentActivity", "No departure date entered");
			}
			
			//arrival date
			if(!arrivalEditText.getText().toString().equals("")) {
				arrivalDate = arrivalEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
				Log.d("SignUpStudentActivity", "No arrival date entered");
			}
			
			if(maleRadio.isChecked()) {
				isMale = true;
				isFemale = false;
				isOther = false;
			}
			else if(femaleRadio.isChecked()) {
				isMale = false;
				isFemale = true;
				isOther = false;
			}
			else {
				isMale = false;
				isFemale = false;
				isOther = true;
			}
				
			
			//smoking or non-smoking
			if(nonSmoking.isChecked()) {
				allowsSmoking = false;
			}
			else {
				allowsSmoking = true;
			}
			
			if(dogsCheckBox.isChecked()) {
				noDogs = true;
			}
			else {
				noDogs = false;
			}
			
			if(catsCheckBox.isChecked()) {
				noCats = true;
			}
			else {
				noCats = false;
			}
			
			if(fishCheckBox.isChecked()) {
				noFish = true;
			}
			else {
				noFish = false;
			}
			
			if(birdsCheckBox.isChecked()) {
				noBirds = true;
			}
			else {
				noBirds = false;
			}
			
			if(rabbitsCheckBox.isChecked()) {
				noRabbits = true;
			}
			else {
				noRabbits = false;
			}
			
			if(horsesCheckBox.isChecked()) {
				noHorses = true;
			}
			else {
				noHorses = false;
			}
			
			//other animals
			otherAnimals = otherAnimalsEditText.getText().toString();
			
			if(peanutsCheckBox.isChecked()) {
				noPeanuts = true;
			}
			else {
				noPeanuts = false;
			}
			
			if(treeNutsCheckBox.isChecked()) {
				noTreeNuts = true;
			}
			else {
				noTreeNuts = false;
			}
			
			if(milkCheckBox.isChecked()) {
				noMilk = true;
			}
			else {
				noMilk = false;
			}
			
			if(eggsCheckBox.isChecked()) {
				noEggs = true;
			}
			else {
				noEggs = false;
			}
			
			if(wheatCheckBox.isChecked()) {
				noWheat = true;
			}
			else {
				noWheat = false;
			}
			
			if(soyCheckBox.isChecked()) {
				noSoy = true;
			}
			else {
				noSoy = false;
			}
			
			if(fishFoodCheckBox.isChecked()) {
				noFishFood = true;
			}
			else {
				noFishFood = false;
			}
			
			if(shellfishCheckBox.isChecked()) {
				noShellfish = true;
			}
			else {
				noShellfish = false;
			}
			
			otherAllergies = otherAllergiesEditText.getText().toString();
			
			numKids = Integer.parseInt(numKidsEditText.getText().toString());
			
			if(maleKidCheckBox.isChecked()) {
				maleKidOk = true;
			}
			else {
				maleKidOk = false;
			}
			
			if(femaleKidCheckBox.isChecked()) {
				femaleKidOk = true;
			}
			else {
				femaleKidOk = false;
			}
			
			if(otherKidCheckBox.isChecked()) {
				otherKidOk = true;
			}
			else {
				otherKidOk = false;
			}
			
			numAdults = Integer.parseInt(numAdultsEditText.getText().toString());
			
			if(maleAdultCheckBox.isChecked()) {
				maleAdultOk = true;
			}
			else {
				maleAdultOk = false;
			}
			
			if(femaleAdultCheckBox.isChecked()) {
				femaleAdultOk = true;
			}
			else {
				femaleAdultOk = false;
			}
			
			if(otherAdultCheckBox.isChecked()) {
				otherAdultOk = true;
			}
			else {
				otherAdultOk = false;
			}
			
			if(!personalStatementEditText.getText().toString().equals("")) {
				personalStatement = personalStatementEditText.getText().toString();
			}
			else {
				allFieldsValid = false;
			}
			
			
			if(allFieldsValid) {
				//push information to the database
				ParseUser student = new ParseUser();
				
				/*ParseFile profilePictureParse = new ParseFile("profilePicture.png", profilePicture);
				profilePictureParse.saveInBackground();
				student.put("profilePicture", profilePictureParse);*/
				
				student.setUsername(username);
				student.setPassword(password);
				student.setEmail(email);
				
				student.put("userType", "student");
				student.put("fullName", fullName);
				student.put("phone", phone);
				student.put("address", address);
				student.put("studentID", studentID);
				student.put("departureDate", departureDate);
				student.put("arrivalDate", arrivalDate);
				student.put("allowsSmoking", allowsSmoking);
				student.put("isMale", isMale);
				student.put("isFemale", isFemale);
				student.put("isOther", isOther);
				student.put("noDogs", noDogs);
				student.put("noCats", noCats);
				student.put("noFish", noFish);
				student.put("noBirds", noBirds);
				student.put("noRabbits", noRabbits);
				student.put("noHorses", noHorses);
				student.put("otherAnimals", otherAnimals);
				student.put("noPeanuts", noPeanuts);
				student.put("noTreeNuts", noTreeNuts);
				student.put("noMilk", noMilk);
				student.put("noEggs", noEggs);
				student.put("noWheat", noWheat);
				student.put("noSoy", noSoy);
				student.put("noFishFood", noFishFood);
				student.put("noShellfish", noShellfish);
				student.put("otherAllergies", otherAllergies);
				student.put("numKidsPreferred", numKids);
				student.put("numAdultsPreferred", numAdults);
				student.put("maleKidOk", maleKidOk);
				student.put("femaleKidOk", femaleKidOk);
				student.put("otherKidOk", otherKidOk);
				student.put("maleAdultOk", maleAdultOk);
				student.put("femaleAdultOk", femaleAdultOk);
				student.put("otherAdultOk", otherAdultOk);
				student.put("personalStatement", personalStatement);
				student.saveInBackground();
				
				student.signUpInBackground(new SignUpCallback() {
					@Override
					public void done(ParseException e) {
						if(e == null) {
							
							Intent intent = new Intent(SignUpStudentActivity.this, UploadFamilyPhotoActivity.class);
							startActivity(intent);
						}
						else {
							Context context = getApplicationContext();
							CharSequence text = e.getMessage();
							int duration = Toast.LENGTH_SHORT;
							
							Toast toast = Toast.makeText(context, text, duration);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							
							Log.d("SignUpStudentActivity", e.getMessage());
						}
					}
				});
			}
			else {
				Context context = getApplicationContext();
				CharSequence text = "Please fill in all text fields";
				int duration = Toast.LENGTH_SHORT;
				
				Toast toast = Toast.makeText(context, text, duration);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				
				Log.d("SignUpStudentActivity", "Missing fields");
			}
		}
	};

	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if(takePictureIntent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
			Bundle extras = data.getExtras();
			Bitmap imageBitmap = (Bitmap) extras.get("data");
			studentUserImageView.setImageBitmap(imageBitmap);
			
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
			profilePicture = stream.toByteArray();
			
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_up_student, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
