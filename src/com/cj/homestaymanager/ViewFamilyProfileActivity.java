package com.cj.homestaymanager;

import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewFamilyProfileActivity extends ActionBarActivity {

	private static final String FAMILY_TYPE = "family";
	
	private static final int MALE_INDEX = 0;
	private static final int FEMALE_INDEX = 1;
	private static final int OTHER_INDEX = 2;
	private static final int NO_PREFERENCE_INDEX = 3;
	
	private static final int GENDER_PREFERENCE_SIZE = 4;
	
	private static final String NO_GENDER_PREFERENCE = "No gender preference";
	
	private static final String DOGS = "dog(s)";
	private static final String CATS = "cat(s)";
	private static final String FISH = "fish";
	private static final String BIRDS = "bird(s)";
	private static final String RABBITS = "rabbit(s)";
	private static final String HORSES = "horse(s)";
	
	private static final String ALLOWS_SMOKING = "allows smoking";
	private static final String SMOKING_NOT_ALLOWED = "smoking not allowed";
	
	private Bitmap profilePicture;
	private String familyName;
	private String email;
	private String phoneNumber;
	private String address;
	private Boolean allowsSmoking;
	private ArrayList<String> pets = new ArrayList<String>();
	private Boolean[] genderPreferences = new Boolean[GENDER_PREFERENCE_SIZE];
	String personalStatement;
	String startDate;
	String endDate;
	
	TextView familyNameTextView;
	TextView emailTextView;
	TextView phoneTextView;
	TextView addressTextView;
	TextView availabilityTextView;
	TextView smokingTextView;
	TextView preferredGendersTextView;
	TextView petsTextView;
	TextView personalStatementTextView;
	
	ParseUser currentFamily;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_family_profile);
		
		familyNameTextView = (TextView) findViewById(R.id.viewFamilyProfileFamilyNameTextView);
		personalStatementTextView = (TextView) findViewById(R.id.viewFamilyProfilePersonalStatementTextView);
		emailTextView = (TextView) findViewById(R.id.viewFamilyProfileEmailTextView);
		phoneTextView = (TextView) findViewById(R.id.viewFamilyProfilePhoneTextView);
		addressTextView = (TextView) findViewById(R.id.viewFamilyProfileAddressTextView);
		availabilityTextView = (TextView) findViewById(R.id.viewFamilyProfileAvailabilityTextView);
		smokingTextView = (TextView) findViewById(R.id.viewFamilyProfileSmokingPreferenceTextView);
		preferredGendersTextView = (TextView) findViewById(R.id.viewFamilyProfileGenderPreferenceTextView);
		petsTextView = (TextView) findViewById(R.id.viewFamilyProfilePetsTextView);
		
		Bundle extras = getIntent().getExtras();
		
		if(extras != null) {
			ParseQuery<ParseUser> query = ParseUser.getQuery();
			query.whereEqualTo("username", extras.getString("EXTRA_USERNAME"));
			query.findInBackground(new FindCallback<ParseUser>() {

				@Override
				public void done(List<ParseUser> families, ParseException e) {
					if(e == null) {
						currentFamily = families.get(0);
						if(!currentFamily.getString("userType").equals(FAMILY_TYPE)) {
							//not a family
							Log.d("ViewFamilyProfileActivity", "Not a family user");
						}
						else {
							getProfilePicture(currentFamily);
							familyName = currentFamily.getString("familyName");
							email = currentFamily.getString("email");
							phoneNumber = currentFamily.getString("phone");
							address = currentFamily.getString("address");
							startDate = currentFamily.getString("startDate");
							endDate = currentFamily.getString("endDate");
							allowsSmoking = currentFamily.getBoolean("allowsSmoking");
							getPets(currentFamily);
							getGenderPreferences(currentFamily);
							personalStatement = currentFamily.getString("personalStatement");
							Log.d("ViewFamilyProfileActivity", personalStatement);
							
							updateProfile();
							
						}
					}
					else {
						Log.d("ViewFamilyProfile", "Coudln't find the family D:");
					}
					
				}
				
			});
		}
		else {
			currentFamily = ParseUser.getCurrentUser();
			if(!currentFamily.getString("userType").equals(FAMILY_TYPE)) {
				//not a family
				Log.d("ViewFamilyProfileActivity", "Not a family user");
			}
			else {
				getProfilePicture(currentFamily);
				familyName = currentFamily.getString("familyName");
				email = currentFamily.getString("email");
				phoneNumber = currentFamily.getString("phone");
				address = currentFamily.getString("address");
				startDate = currentFamily.getString("startDate");
				endDate = currentFamily.getString("endDate");
				allowsSmoking = currentFamily.getBoolean("allowsSmoking");
				getPets(currentFamily);
				getGenderPreferences(currentFamily);
				personalStatement = currentFamily.getString("personalStatement");
				Log.d("ViewFamilyProfileActivity", personalStatement);
				
				updateProfile();
				
			}
		}
		
		
	}

	private void getProfilePicture(ParseUser currentFamily) {
		ParseFile picture = (ParseFile)currentFamily.get("profilePicture");
		picture.getDataInBackground(new GetDataCallback() {
			@Override
			public void done(byte[] data, ParseException e) {
				if(e == null) {
					//convert ByteArray to Bitmap
					ImageView profilePictureImageView = (ImageView) findViewById(R.id.viewFamilyProfilePictureImageView);
					profilePictureImageView.setImageBitmap(BitmapFactory.decodeByteArray(data, 0, data.length));
				}
				else {
					Log.d("ViewFamilyProfileActivity", "Couldn't get the picture");
				}	
			}
		});
	}
	
	private void getGenderPreferences(ParseUser currentFamily) {
		//initial values
		for(int i = 0; i < GENDER_PREFERENCE_SIZE; i++)
			genderPreferences[i] = false;
		
		if(currentFamily.getBoolean("preferFemale"))
			genderPreferences[FEMALE_INDEX] = true;
		if(currentFamily.getBoolean("preferMale"))
			genderPreferences[MALE_INDEX] = true;
		if(currentFamily.getBoolean("preferOther"))
			genderPreferences[OTHER_INDEX] = true;
		if(currentFamily.getBoolean("noGenderPreference"))
			genderPreferences[NO_PREFERENCE_INDEX] = true;
	}
	
	private void getPets(ParseUser currentFamily) {
		if(currentFamily.getBoolean("hasDogs"))
			pets.add(DOGS);
		if(currentFamily.getBoolean("hasCats"))
			pets.add(CATS);
		if(currentFamily.getBoolean("hasFish"))
			pets.add(FISH);
		if(currentFamily.getBoolean("hasBirds"))
			pets.add(BIRDS);
		if(currentFamily.getBoolean("hasRabbits"))
			pets.add(RABBITS);
		if(currentFamily.getBoolean("hasHorses"))
			pets.add(HORSES);
	}
	
	private void updateProfile() {
		familyNameTextView.setText("The " + familyName + " Family");
		emailTextView.setText(email);
		phoneTextView.setText("(" + phoneNumber.substring(0, 3) + ") " + phoneNumber.substring(3, 6) + " " + phoneNumber.substring(6, 10));
		addressTextView.setText(address);
		
		availabilityTextView.setText("available " + formatDate(startDate) + " - " + formatDate(endDate));
		
		if(allowsSmoking)
			smokingTextView.setText(ALLOWS_SMOKING);
		else 
			smokingTextView.setText(SMOKING_NOT_ALLOWED);
		
		if(genderPreferences[NO_PREFERENCE_INDEX] == true)
			preferredGendersTextView.setText(NO_GENDER_PREFERENCE);
		else {
			String buffer = "preferred genders: ";
			int preferredCounter = 0;
			
			for(int i = 0; i < 3; i++) {
				if(genderPreferences[i] == true)
					preferredCounter++;
			}
			
			if(genderPreferences[MALE_INDEX] == true) {
				if(preferredCounter > 1) 
					buffer += "male, ";
				else
					buffer += "male";
				preferredCounter--;
			}
				
			if(genderPreferences[FEMALE_INDEX] == true) {
				if(preferredCounter > 1)
					buffer += "female, ";
				else
					buffer += "female";
				preferredCounter--;
			}
				
			if(genderPreferences[OTHER_INDEX] == true) {
				if(preferredCounter > 1)
					buffer += "other, ";
				else
					buffer += "other";
				preferredCounter--;
			}
			
			preferredGendersTextView.setText(buffer);
			
		}
		petsTextView.setText("pets: " + pets.toString().substring(1, pets.toString().length() - 1));
		
		personalStatementTextView.setText("\"" + personalStatement + "\"");
		
	}
	
	private String formatDate(String date) {
		if(date.length() == 8) {
			String buffer = "";
			buffer += date.substring(0,2);
			buffer += "/";
			buffer += date.substring(2,4);
			buffer += "/";
			buffer += date.substring(4, date.length());
			
			return buffer;
		}
		else
			return "Oops, wrong format!";
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_family_profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
