package com.cj.homestaymanager;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.parse.ParseUser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FamilyArrayAdapter extends ArrayAdapter<ParseUser> {
	
	private final Context context;
	private final ArrayList<ParseUser> familiesArrayList;
	
	public FamilyArrayAdapter(Context context, ArrayList<ParseUser> familiesArrayList) {
		super(context, R.layout.family_row, familiesArrayList);
		
		this.context = context;
		this.familiesArrayList = familiesArrayList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View familyRowView = inflater.inflate(R.layout.family_row, parent, false);
		
		TextView familyNameTextView = (TextView) familyRowView.findViewById(R.id.familyRowFamilyNameTextView);
		TextView usernameTextView = (TextView) familyRowView.findViewById(R.id.familyRowUsernameTextView);
		TextView locationTextView = (TextView) familyRowView.findViewById(R.id.familyRowLocationTextView);
		TextView availabilityTextView = (TextView) familyRowView.findViewById(R.id.familyRowAvailabilityTextView);
		TextView personalStatementTextView = (TextView) familyRowView.findViewById(R.id.familyRowPersonalStatementTextView);
		
		Button saveButton = (Button) familyRowView.findViewById(R.id.familyRowSaveButton);
		saveButton.setOnClickListener((android.view.View.OnClickListener) saveButtonListener);
		
		familyNameTextView.setText(familiesArrayList.get(position).getString("familyName"));
		usernameTextView.setText(familiesArrayList.get(position).getUsername());
		locationTextView.setText(familiesArrayList.get(position).getString("address"));
		availabilityTextView.setText(formatDate(familiesArrayList.get(position).getString("startDate")) + " - " + 
				formatDate(familiesArrayList.get(position).getString("endDate")));
		personalStatementTextView.setText("\"" + familiesArrayList.get(position).getString("personalStatement") + "\"");
		
		return familyRowView;
		
	}
	
	public OnClickListener saveButtonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			
			LinearLayout linearLayout = (LinearLayout) v.getParent().getParent();
			
			TextView familyUsernameTextView = (TextView) linearLayout.findViewById(R.id.familyRowUsernameTextView);
			
			String familyUsername = familyUsernameTextView.getText().toString();
			
			ParseUser student = ParseUser.getCurrentUser();
			
			JSONArray savedFamilies = new JSONArray();
			
			if(student.getJSONArray("savedFamilies") == null) {
				savedFamilies.put(familyUsername);
				student.put("savedFamilies", savedFamilies);
			}
			else {
				savedFamilies = student.getJSONArray("savedFamilies");
				boolean hasFamilyAlready = false;
				
				for(int i = 0; i < savedFamilies.length(); i++) {
					try {
						if(savedFamilies.getString(i).equals(familyUsername))
							hasFamilyAlready = true;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(!hasFamilyAlready) {
					savedFamilies.put(familyUsername);
					student.put("savedFamilies", savedFamilies);
				}
				
			}
			student.saveInBackground();
		}
		
	};
	
	private String formatDate(String date) {
		if(date.length() == 8) {
			String buffer = "";
			buffer += date.substring(0,2);
			buffer += "/";
			buffer += date.substring(2,4);
			buffer += "/";
			buffer += date.substring(4, date.length());
			
			return buffer;
		}
		else
			return "Oops, wrong format!";
	}

}












