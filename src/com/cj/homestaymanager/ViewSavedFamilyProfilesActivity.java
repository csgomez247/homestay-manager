package com.cj.homestaymanager;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Build;

public class ViewSavedFamilyProfilesActivity extends ActionBarActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_saved_family_profiles);

		ArrayList<ParseUser> familyInfo = new ArrayList<ParseUser>();
		
		final RemovableFamilyArrayAdapter listAdapter = new RemovableFamilyArrayAdapter(this, familyInfo);
		
		final ListView familiesListView = (ListView) findViewById(R.id.viewSavedFamiliesListView);
		familiesListView.setOnItemClickListener(familiesListViewItemListener);
		
		familiesListView.setAdapter(listAdapter);
		
		ParseUser currentStudent = ParseUser.getCurrentUser();
		
		JSONArray savedFamilies = currentStudent.getJSONArray("savedFamilies");
		
		if(savedFamilies == null) {
			Toast.makeText(getApplicationContext(), "No families saved!", Toast.LENGTH_LONG).show();
			Intent intent = new Intent(ViewSavedFamilyProfilesActivity.this, StudentUserMenuActivity.class);
			startActivity(intent);
		}
		else {
			
			for(int i = 0; i < savedFamilies.length(); i++) {
				ParseQuery<ParseUser> query = ParseUser.getQuery();
				try {
					query.whereEqualTo("username", savedFamilies.getString(i));
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				query.findInBackground(new FindCallback<ParseUser>() {
					  public void done(List<ParseUser> objects, ParseException e) {
					    if (e == null) {
					        listAdapter.add(objects.get(0));
						} 
					    else {
					    	Toast.makeText(getApplicationContext(), "Couldn't find families!", Toast.LENGTH_LONG).show();
						}
					  }
				});
			}
			
		}
	}
	
	public OnItemClickListener familiesListViewItemListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
			
			LinearLayout linearLayout = (LinearLayout) v;
			
			TextView usernameTextView = (TextView) linearLayout.findViewById(R.id.removableFamilyRowUsernameTextView);
			
			Intent intent = new Intent(ViewSavedFamilyProfilesActivity.this, ViewLimitedProfileActivity.class);
			intent.putExtra("EXTRA_USERNAME", usernameTextView.getText().toString());
			startActivity(intent);
			
		}	
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_saved_family_profiles, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
