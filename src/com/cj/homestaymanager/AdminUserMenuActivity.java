package com.cj.homestaymanager;

import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import android.os.Build;

public class AdminUserMenuActivity extends ActionBarActivity {

	Button manageStudentsButton;
	Button manageFamiliesButton;
	Button evalPairsButton;
	Button logOutButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_admin_user_menu);
		
		manageStudentsButton = (Button) findViewById(R.id.adminUserMenuManageStudentsButton);
		manageStudentsButton.setOnClickListener((android.view.View.OnClickListener) manageStudentsListener);
		
		manageFamiliesButton = (Button) findViewById(R.id.adminUserMenuManageFamiliesButton);
		manageFamiliesButton.setOnClickListener((android.view.View.OnClickListener) manageFamiliesListener);
		
		evalPairsButton = (Button) findViewById(R.id.adminUserMenuEvaluatePairingsButton);
		evalPairsButton.setOnClickListener((android.view.View.OnClickListener) evalPairsListener);
		
		logOutButton = (Button) findViewById(R.id.adminUserMenuLogOutButton);
		logOutButton.setOnClickListener((android.view.View.OnClickListener) logOutListener);
		
	}
	
	public OnClickListener manageStudentsListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent(AdminUserMenuActivity.this, ManageStudentsActivity.class);
			startActivity(intent);
		}
		
	};
	
	public OnClickListener manageFamiliesListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(AdminUserMenuActivity.this, ManageFamiliesActivity.class);
			startActivity(intent);
		}
		
	};
	
	public OnClickListener evalPairsListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(AdminUserMenuActivity.this, EvalPairsActivity.class);
			startActivity(intent);
		}
		
	};
	
	public OnClickListener logOutListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			ParseUser currentUser = ParseUser.getCurrentUser();
			
			String currentUserName = currentUser.getUsername();
			ParseUser.logOut();
			
			Context context = getApplicationContext();
			CharSequence text = "Logging out " + currentUserName;
			int duration = Toast.LENGTH_SHORT;
			
			Toast toast = Toast.makeText(context, text, duration);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			
			Intent intent = new Intent(AdminUserMenuActivity.this, MainMenuActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
		}
		
	};

	@Override
	public void onBackPressed() {
		ParseUser currentUser = ParseUser.getCurrentUser();
		String currentUserName = currentUser.getUsername();
		
		ParseUser.logOut();
		
		Context context = getApplicationContext();
		CharSequence text = "Logging out " + currentUserName;
		int duration = Toast.LENGTH_SHORT;
		
		Toast toast = Toast.makeText(context, text, duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		
		Intent intent = new Intent(AdminUserMenuActivity.this, MainMenuActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		//super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.admin_user_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
