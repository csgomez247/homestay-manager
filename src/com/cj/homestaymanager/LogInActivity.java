package com.cj.homestaymanager;

import java.util.List;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LogInActivity extends ActionBarActivity {

	TextView usernameTextView;
	EditText usernameEditText;
	String username;
	
	TextView passwordTextView;
	EditText passwordEditText;
	String password;
	
	Button logInButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log_in);
        //Parse.initialize(this, "2aQ6dNiMtTKPzrCKxT08icDP0laebuljRZB9IeRK", "rLGzN5C6M3EyTIQYebCWp2L6nXbRfwV3TEesMckg");
		
        
        usernameTextView = (TextView) findViewById(R.id.usernameLogInTextView);
        usernameEditText = (EditText) findViewById(R.id.usernameLogInEditText);
        
        passwordTextView = (TextView) findViewById(R.id.passwordLogInTextView);
        passwordEditText = (EditText) findViewById(R.id.passwordLogInEditText);
        
        logInButton = (Button) findViewById(R.id.LogInActivity_logInButton);
        logInButton.setOnClickListener((android.view.View.OnClickListener) logInButtonListener);
	}
	
	public OnClickListener logInButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(!usernameEditText.getText().toString().equals("") &&
					!passwordEditText.getText().toString().equals("")) {
				
				ParseUser.logInInBackground(usernameEditText.getText().toString(), passwordEditText.getText().toString(), new LogInCallback() {
					@Override
					public void done(ParseUser user, ParseException e) {
						if(user != null) {
							if(user.getString("userType").equals("family")) {
								Intent intent = new Intent(LogInActivity.this, FamilyUserMenuActivity.class);
								startActivity(intent);
							}
							else {
								Intent intent = new Intent(LogInActivity.this, StudentUserMenuActivity.class);
								startActivity(intent);
							}
						}
						else {
							Context context = getApplicationContext();
							CharSequence text = e.getMessage();
							int duration = Toast.LENGTH_SHORT;
							
							Toast toast = Toast.makeText(context, text, duration);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							
							Log.d("LogInActivity", e.getMessage());
						}
						
					}
				});
			}
			else {
				Context context = getApplicationContext();
				CharSequence text = "Please fill in all fields";
				int duration = Toast.LENGTH_SHORT;
				
				Toast toast = Toast.makeText(context, text, duration);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				
				Log.d("LogInActivity", "Username or Password missing");
			}
		}
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.log_in, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
