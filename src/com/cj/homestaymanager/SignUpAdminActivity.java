package com.cj.homestaymanager;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class SignUpAdminActivity extends ActionBarActivity {

	protected static final String ADMIN_KEY = "ecs160";
	TextView usernameTextView;
	EditText usernameEditText;
	String username;
	
	TextView passwordTextView;
	EditText passwordEditText;
	String password;
	
	TextView emailTextView;
	EditText emailEditText;
	String email;
	
	TextView adminKeyTextView;
	EditText adminKeyEditText;
	String adminKey;
	
	Button signUpButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up_admin);
		//Parse.initialize(this, "2aQ6dNiMtTKPzrCKxT08icDP0laebuljRZB9IeRK", "rLGzN5C6M3EyTIQYebCWp2L6nXbRfwV3TEesMckg");
		
		usernameTextView = (TextView) findViewById(R.id.signUpAdminUsernameTextView);
		usernameEditText = (EditText) findViewById(R.id.signUpAdminUsernameEditText);
		
		passwordTextView = (TextView) findViewById(R.id.signUpAdminPasswordTextView);
		passwordEditText = (EditText) findViewById(R.id.signUpAdminPasswordEditText);
		
		emailTextView = (TextView) findViewById(R.id.signUpAdminEmailTextView);
		emailEditText = (EditText) findViewById(R.id.signUpAdminEmailEditText);
		
		adminKeyTextView = (TextView) findViewById(R.id.signUpAdminKeyTextView);
		adminKeyEditText = (EditText) findViewById(R.id.signUpAdminKeyEditText);
		
		signUpButton = (Button) findViewById(R.id.signUpAdminButton);
		signUpButton.setOnClickListener((android.view.View.OnClickListener) signUpButtonListener);
		
	}

	public OnClickListener signUpButtonListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			boolean allFieldsValid = true;
			
			username = usernameEditText.getText().toString();
			if(username.equals(""))
				allFieldsValid = false;
			
			password = passwordEditText.getText().toString();
			if(password.equals(""))
				allFieldsValid = false;
			
			email = emailEditText.getText().toString();
			if(email.equals(""))
				allFieldsValid = false;
			
			adminKey = adminKeyEditText.getText().toString();
			if(adminKey.equals(""))
				allFieldsValid = false;
			
			if(allFieldsValid == true) {
				if(adminKey.equals(ADMIN_KEY)) {
					ParseUser admin = new ParseUser();
					
					admin.setUsername(username);
					admin.setPassword(password);
					admin.setEmail(email);
					
					admin.put("userType", "admin");
					
					admin.signUpInBackground(new SignUpCallback() {
						@Override
						public void done(ParseException e) {
							if(e == null) {
								Intent intent = new Intent(SignUpAdminActivity.this, AdminUserMenuActivity.class);
								startActivity(intent);
							}
							else {
								Context context = getApplicationContext();
								CharSequence text = e.getMessage();
								int duration = Toast.LENGTH_SHORT;
								
								Toast toast = Toast.makeText(context, text, duration);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
								
								Log.d("SignUpAdminActivity", e.getMessage());
							}
						}
					});
				}
				else
					showToast("Wrong admin key");
			}
			else
				showToast("Please fill in all fields");
			
		}
		
	};
	
	private void showToast(String s) {
		Context context = getApplicationContext();
		CharSequence text = s;
		int duration = Toast.LENGTH_SHORT;
		
		Toast toast = Toast.makeText(context, text, duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_up_admin, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
