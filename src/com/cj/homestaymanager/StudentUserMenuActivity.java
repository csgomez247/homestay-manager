package com.cj.homestaymanager;

import com.parse.Parse;
import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class StudentUserMenuActivity extends ActionBarActivity {

	@Override
	public void onBackPressed() {
		ParseUser currentUser = ParseUser.getCurrentUser();
		String currentUserName = currentUser.getUsername();
		
		ParseUser.logOut();
		
		Context context = getApplicationContext();
		CharSequence text = "Logging out " + currentUserName;
		int duration = Toast.LENGTH_SHORT;
		
		Toast toast = Toast.makeText(context, text, duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		
		Intent intent = new Intent(StudentUserMenuActivity.this, MainMenuActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		//super.onBackPressed();
	}

	Button placementWizardButton;
	Button manualSelectionButton;
	Button viewSavedFamilyProfilesButton;
	Button viewStudentProfileButton;
	Button editStudentProfileButton;
	Button logOutStudentButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_student_user_menu);
		//Parse.initialize(this, "2aQ6dNiMtTKPzrCKxT08icDP0laebuljRZB9IeRK", "rLGzN5C6M3EyTIQYebCWp2L6nXbRfwV3TEesMckg");
		
		placementWizardButton = (Button) findViewById(R.id.placementWizardButton);
		placementWizardButton.setOnClickListener((android.view.View.OnClickListener) placementWizardButtonListener);
		
		manualSelectionButton = (Button) findViewById(R.id.manualSelectionButton);
		manualSelectionButton.setOnClickListener((android.view.View.OnClickListener) manualSelectionButtonListener);
		
		viewSavedFamilyProfilesButton = (Button) findViewById(R.id.viewSavedProfilesButton);
		viewSavedFamilyProfilesButton.setOnClickListener((android.view.View.OnClickListener) viewSavedFamilyProfilesListener);
		
		viewStudentProfileButton = (Button) findViewById(R.id.viewStudentProfileButton);
		viewStudentProfileButton.setOnClickListener((android.view.View.OnClickListener) viewStudentProfileButtonListener);
		
		editStudentProfileButton = (Button) findViewById(R.id.editStudentProfileButton);
		editStudentProfileButton.setOnClickListener((android.view.View.OnClickListener) editStudentProfileButtonListener);
		
		logOutStudentButton = (Button) findViewById(R.id.logOutStudentButton);
		logOutStudentButton.setOnClickListener((android.view.View.OnClickListener) logOutStudentButtonListener);
		
	}
	
	public OnClickListener placementWizardButtonListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent(StudentUserMenuActivity.this, PlacementWizardActivity.class);
			startActivity(intent);
		}
	};
	
	public OnClickListener manualSelectionButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(StudentUserMenuActivity.this, ManualSelectionActivity.class);
			startActivity(intent);	
		}
	};
	
	public OnClickListener viewSavedFamilyProfilesListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(StudentUserMenuActivity.this, ViewSavedFamilyProfilesActivity.class);
			startActivity(intent);	
		}
	};
	
	public OnClickListener viewStudentProfileButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(StudentUserMenuActivity.this, ViewStudentProfileActivity.class);
			startActivity(intent);	
		}
	};
	
	public OnClickListener editStudentProfileButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(StudentUserMenuActivity.this, EditStudentProfileActivity.class);
			startActivity(intent);	
		}
	};
	
	public OnClickListener logOutStudentButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			
			ParseUser currentUser = ParseUser.getCurrentUser();
			String currentUserName = currentUser.getUsername();
			
			ParseUser.logOut();
			
			Context context = getApplicationContext();
			CharSequence text = "Logging out " + currentUserName;
			int duration = Toast.LENGTH_SHORT;
			
			Toast toast = Toast.makeText(context, text, duration);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			
			Intent intent = new Intent(StudentUserMenuActivity.this, MainMenuActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);	
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.student_user_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
