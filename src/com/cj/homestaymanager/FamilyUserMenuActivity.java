package com.cj.homestaymanager;

import com.parse.Parse;
import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class FamilyUserMenuActivity extends ActionBarActivity {

	Button viewMyProfileButton;
	Button editMyProfileButton;
	Button logOutButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_family_user_menu);
		//Parse.initialize(this, "2aQ6dNiMtTKPzrCKxT08icDP0laebuljRZB9IeRK", "rLGzN5C6M3EyTIQYebCWp2L6nXbRfwV3TEesMckg");
		
		viewMyProfileButton = (Button) findViewById(R.id.viewMyFamilyProfileButton);
		viewMyProfileButton.setOnClickListener((android.view.View.OnClickListener) viewMyProfileListener);
		
		editMyProfileButton = (Button) findViewById(R.id.editMyFamilyProfileButton);
		editMyProfileButton.setOnClickListener((android.view.View.OnClickListener) editMyProfileListener);
		
		logOutButton = (Button) findViewById(R.id.familyUserLogoutButton);
		logOutButton.setOnClickListener((android.view.View.OnClickListener) logOutListener);
	}

	public OnClickListener viewMyProfileListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(FamilyUserMenuActivity.this, ViewFamilyProfileActivity.class);
			startActivity(intent);
		}
	};
	
	public OnClickListener editMyProfileListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(FamilyUserMenuActivity.this, EditFamilyProfileActivity.class);
			startActivity(intent);
		}
	};
	
	@Override
	public void onBackPressed() {
		ParseUser currentUser = ParseUser.getCurrentUser();
		String currentUserName = currentUser.getUsername();
		
		ParseUser.logOut();
		
		Context context = getApplicationContext();
		CharSequence text = "Logging out " + currentUserName;
		int duration = Toast.LENGTH_SHORT;
		
		Toast toast = Toast.makeText(context, text, duration);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		
		Intent intent = new Intent(FamilyUserMenuActivity.this, MainMenuActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		//super.onBackPressed();
	}

	public OnClickListener logOutListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			
			ParseUser currentUser = ParseUser.getCurrentUser();
			
			String currentUserName = currentUser.getUsername();
			ParseUser.logOut();
			
			Context context = getApplicationContext();
			CharSequence text = "Logging out " + currentUserName;
			int duration = Toast.LENGTH_SHORT;
			
			Toast toast = Toast.makeText(context, text, duration);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			
			Intent intent = new Intent(FamilyUserMenuActivity.this, MainMenuActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
		}
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.family_user_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
