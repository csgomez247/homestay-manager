package com.cj.homestaymanager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Build;

public class PlacementWizardActivity extends ActionBarActivity {

	ArrayList<ParseUser> fams = new ArrayList<ParseUser>();
	ParseUser currentUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_placement_wizard);
		
		currentUser = ParseUser.getCurrentUser();
		
		ArrayList<ParseUser> familyInfo = new ArrayList<ParseUser>();
		
		final FamilyArrayAdapter listAdapter = new FamilyArrayAdapter(this, familyInfo);
		
		final ListView familiesListView = (ListView) findViewById(R.id.placementWizardListView);
		familiesListView.setOnItemClickListener(familiesListViewItemListener);
		
		familiesListView.setAdapter(listAdapter);
		
		final ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo("userType", "family");
		query.findInBackground(new FindCallback<ParseUser>() {
			@Override
			public void done(List<ParseUser> fs, ParseException e) {
				if(e == null) {
					Collections.sort(fs, new FamilyComparator());
					
					for(ParseUser family : fs) {
						listAdapter.add(family);
					}
				}
				else {
					Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
				}
			}
		});
		
	}

	class FamilyComparator implements Comparator<ParseUser> {

		@Override
		public int compare(ParseUser f1, ParseUser f2) {
			ParseUser student = ParseUser.getCurrentUser();
			
			if(calculatePoints(f1, student) > calculatePoints(f2, student))
				return -1;
			
			if(calculatePoints(f1, student) == calculatePoints(f2, student))
				return 1;
			
			return 0;
			
		}
		
	}
	
	public OnItemClickListener familiesListViewItemListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
			
			LinearLayout linearLayout = (LinearLayout) v;
			
			TextView usernameTextView = (TextView) linearLayout.findViewById(R.id.familyRowUsernameTextView);
			
			Intent intent = new Intent(PlacementWizardActivity.this, ViewLimitedProfileActivity.class);
			intent.putExtra("EXTRA_USERNAME", usernameTextView.getText().toString());
			startActivity(intent);
			
		}	
	};
	
	private int calculatePoints(ParseUser family, ParseUser student) {
		
		int points = 0;
		
		/*
		 * DATES
		 */
		int studentArrivalDate = makeDateComparable(student.getString("arrivalDate"));
		int studentDepartureDate = makeDateComparable(student.getString("departureDate"));
		
		int familyStartDate = makeDateComparable(family.getString("startDate"));
		int familyEndDate = makeDateComparable(family.getString("endDate"));
		
		if(familyStartDate <= studentArrivalDate && familyEndDate >= studentDepartureDate)
			points+=500;
		
		/*
		 * SMOKING
		 */
		if(family.getBoolean("allowsSmoking") == student.getBoolean("allowsSmoking"))
			points++;
		
		/*
		 * HAS/HATES ANIMAL
		 */
		if(family.getBoolean("hasDogs") && student.getBoolean("noDogs"))
			points--;
		if(family.getBoolean("hasBirds") && student.getBoolean("noBirds"))
			points--;
		if(family.getBoolean("hasCats") && student.getBoolean("noCats"))
			points--;
		if(family.getBoolean("hasFish") && student.getBoolean("noFish"))
			points--;
		if(family.getBoolean("hasHorses") && student.getBoolean("noHorses"))
			points--;
		if(family.getBoolean("hasRabbits") && student.getBoolean("noRabbits"))
			points--;
		
		/*
		 * ALLERGIC TO / EATS
		 */
		if(student.getBoolean("noPeanuts") && family.getBoolean("eatsPeanuts"))
			points--;
		if(student.getBoolean("noTreeNuts") && family.getBoolean("eatsTreeNuts"))
			points--;
		if(student.getBoolean("noMilk") && family.getBoolean("eatsMilk"))
			points--;
		if(student.getBoolean("noEggs") && family.getBoolean("eatsEggs"))
			points--;
		if(student.getBoolean("noWheat") && family.getBoolean("eatsWheat"))
			points--;
		if(student.getBoolean("noSoy") && family.getBoolean("eatsSoy"))
			points--;
		if(student.getBoolean("noFishFood") && family.getBoolean("eatsFish"))
			points--;
		if(student.getBoolean("noShellfish") && family.getBoolean("eatsShellfish"))
			points--;
		
		/*
		 * GENDER IDENTITY
		 */
		if(student.getBoolean("isMale") && family.getBoolean("preferMale"))
			points++;
		if(student.getBoolean("isFemale") && family.getBoolean("preferFemale"))
			points++;
		if(student.getBoolean("isOther") && family.getBoolean("preferOther"))
			points++;
		if(family.getBoolean("noGenderPreference"))
			points++;
		
		/*
		 * FAMILY STRUCTURE
		 */
		if(family.getInt("numKidsInFamily") <= student.getInt("numKidsPreferred"))
			points++;
		if(family.getInt("numAdultsInFamily") <= student.getInt("numAdultsPreferred"))
			points++;
		if(family.getBoolean("hasMaleKids") && student.getBoolean("maleKidOk"))
			points++;
		if(family.getBoolean("hasFemaleKids") && student.getBoolean("femaleKidOk"))
			points++;
		if(family.getBoolean("hasOtherKids") && student.getBoolean("otherKidOk"))
			points++;
		if(family.getBoolean("hasMaleAdults") && student.getBoolean("maleAdultOk"))
			points++;
		if(family.getBoolean("hasFemaleAdults") && student.getBoolean("femaleAdultOk"))
			points++;
		if(family.getBoolean("hasOtherAdults") && student.getBoolean("otherAdultOK"))
			points++;
		
		
		
		return points;
	}
	
	private int makeDateComparable(String date) {
		String buffer = "";
		
		buffer += date.substring(4, date.length());
		buffer += date.substring(0, 2);
		buffer += date.substring(2, 4);
		
		return Integer.parseInt(buffer);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.placement_wizard, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
