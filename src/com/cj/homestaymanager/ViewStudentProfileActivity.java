package com.cj.homestaymanager;

import java.util.ArrayList;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Build;

public class ViewStudentProfileActivity extends ActionBarActivity {
	
	TextView fullNameTextView;
	TextView emailTextView;
	TextView phoneTextView;
	TextView addressTextView;
	TextView availabilityTextView;
	TextView studentIDTextView;
	TextView smokingTextView;
	TextView petComfortTextView;
	TextView allergiesTextView;
	TextView kidsComfortTextView;
	TextView adultsComfortTextView;
	TextView personalStatementTextView;
	
	String fullName;
	String email;
	String phone;
	String address;
	String studentID;
	Boolean allowsSmoking;
	ArrayList<String> petDiscomfort = new ArrayList<String>();
	ArrayList<String> allergies = new ArrayList<String>();
	String personalStatement;
	String arrivalDate;
	String departureDate;
	
	boolean maleKidOK;
	boolean femaleKidOK;
	boolean otherKidOK;
	int numKids = 99;
	
	boolean maleAdultOK;
	boolean femaleAdultOK;
	boolean otherAdultOK;
	int numAdults = 98;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_student_profile);
		
		fullNameTextView = (TextView) findViewById(R.id.viewStudentProfileFullNameTextView);
		personalStatementTextView = (TextView) findViewById(R.id.viewStudentProfilePersonalStatementTextView);
		emailTextView = (TextView) findViewById(R.id.viewStudentProfileEmailTextView);
		phoneTextView = (TextView) findViewById(R.id.viewStudentProfilePhoneTextView);
		addressTextView = (TextView) findViewById(R.id.viewStudentProfileAddressTextView);
		availabilityTextView = (TextView) findViewById(R.id.viewStudentProfileAvailabilityTextView);
		studentIDTextView = (TextView) findViewById(R.id.viewStudentProfileSIDTextView);
		smokingTextView = (TextView) findViewById(R.id.viewStudentProfileSmokingTextView);
		petComfortTextView = (TextView) findViewById(R.id.viewStudentProfilePetComfortTextView);
		allergiesTextView = (TextView) findViewById(R.id.viewStudentProfileAllergiesTextView);
		kidsComfortTextView = (TextView) findViewById(R.id.viewStudentProfileKidsComfortTextView);
		adultsComfortTextView = (TextView) findViewById(R.id.viewStudentProfileAdultsComfortTextView);	
		
		ParseUser currentStudent = ParseUser.getCurrentUser();
		if(!currentStudent.getString("userType").equals("student")) {
			//not a family
			Log.d("ViewStudentProfileActivity", "Not a student user");
		}
		else {
			getProfilePicture(currentStudent);
			fullName = currentStudent.getString("fullName");
			email = currentStudent.getString("email");
			phone = currentStudent.getString("phone");
			address = currentStudent.getString("address");
			arrivalDate = currentStudent.getString("arrivalDate");
			departureDate = currentStudent.getString("departureDate");
			studentID = currentStudent.getString("studentID");
			allowsSmoking = currentStudent.getBoolean("allowsSmoking");
			getPetDiscomfort(currentStudent);
			getAllergies(currentStudent);
			getFamilyComfortPreferences(currentStudent);
			personalStatement = currentStudent.getString("personalStatement");
			updateProfile(currentStudent);
			
		}
		
	}
	
	private void getProfilePicture(ParseUser currentStudent) {
		ParseFile picture = (ParseFile)currentStudent.get("profilePicture");
		picture.getDataInBackground(new GetDataCallback() {
			@Override
			public void done(byte[] data, ParseException e) {
				if(e == null) {
					//convert ByteArray to Bitmap
					ImageView profilePictureImageView = (ImageView) findViewById(R.id.viewStudentProfileProfilePictureImageView);
					profilePictureImageView.setImageBitmap(BitmapFactory.decodeByteArray(data, 0, data.length));
				}
				else {
					Log.d("ViewStudentProfileActivity", "Couldn't get the picture");
				}	
			}
		});
	}
	
	private void getAllergies(ParseUser currentStudent) {
		if(currentStudent.getBoolean("noPeanuts"))
			allergies.add("peanuts");
		if(currentStudent.getBoolean("noMilk"))
			allergies.add("milk");
		if(currentStudent.getBoolean("noWheat"))
			allergies.add("wheat");
		if(currentStudent.getBoolean("noFishFood"))
			allergies.add("fish");
		if(currentStudent.getBoolean("noTreeNuts"))
			allergies.add("tree nuts");
		if(currentStudent.getBoolean("noEggs"))
			allergies.add("eggs");
		if(currentStudent.getBoolean("noSoy"))
			allergies.add("soy");
		if(currentStudent.getBoolean("noShellfish"))
			allergies.add("shellfish");
	}
	
	private void getFamilyComfortPreferences(ParseUser currentStudent) {
		numKids = currentStudent.getInt("numKidsPreferred");
		numAdults = currentStudent.getInt("numAdultsPreferred");
		
		maleKidOK = currentStudent.getBoolean("maleKidOk");
		femaleKidOK = currentStudent.getBoolean("femaleKidOk");
		otherKidOK = currentStudent.getBoolean("otherKidOk");
		
		maleAdultOK = currentStudent.getBoolean("maleAdultOk");
		femaleAdultOK = currentStudent.getBoolean("femaleAdultOk");
		otherAdultOK = currentStudent.getBoolean("otherAdultOk");
	}
	
	private void getPetDiscomfort(ParseUser currentStudent) {
		if(currentStudent.getBoolean("noDogs"))
			petDiscomfort.add("dog(s)");
		if(currentStudent.getBoolean("noCats"))
			petDiscomfort.add("cat(s)");
		if(currentStudent.getBoolean("noFish"))
			petDiscomfort.add("fish");
		if(currentStudent.getBoolean("noBirds"))
			petDiscomfort.add("bird(s)");
		if(currentStudent.getBoolean("noRabbits"))
			petDiscomfort.add("rabbit(s)");
		if(currentStudent.getBoolean("noHorses"))
			petDiscomfort.add("horse(s)");
	}

	private void updateProfile(ParseUser currentStudent) {
		fullNameTextView.setText(fullName);
		emailTextView.setText(email);
		phoneTextView.setText("(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + " " + phone.substring(6, 10));
		addressTextView.setText(address);
		availabilityTextView.setText("available " + formatDate(arrivalDate) + " to " + formatDate(departureDate));
		studentIDTextView.setText("Student ID: " + studentID);
		if(allowsSmoking)
			smokingTextView.setText("allows smoking");
		else
			smokingTextView.setText("does not allow smoking");
		petComfortTextView.setText("undesired pets: " + petDiscomfort.toString().substring(1, petDiscomfort.toString().length() - 1));
		allergiesTextView.setText("allergies: " + allergies.toString().substring(1, allergies.toString().length() - 1));
		
		kidsComfortTextView.setText("comfortable with a maximum of " + numKids + " kids (" + 
				formatGenderPreference(maleKidOK, femaleKidOK, otherKidOK) + ")");
		
		adultsComfortTextView.setText("comfortable with a maximum of " + numAdults + " adults (" +
				formatGenderPreference(maleAdultOK, femaleAdultOK, otherAdultOK) + ")");
		
		personalStatementTextView.setText("\"" + personalStatement + "\"");
		
	}
	
	private String formatDate(String date) {
		if(date.length() == 8) {
			String buffer = "";
			buffer += date.substring(0,2);
			buffer += "/";
			buffer += date.substring(2,4);
			buffer += "/";
			buffer += date.substring(4, date.length());
			
			return buffer;
		}
		else
			return "Oops, wrong format!";
	}
	
	private String formatGenderPreference(boolean maleOK, boolean femaleOK, boolean otherOK) {
		String buffer = "";
		int okCounter = 0;
		
		if(maleOK)
			okCounter++;
		if(femaleOK)
			okCounter++;
		if(otherOK)
			okCounter++;
		
		if(maleOK) {
			if(okCounter > 1)
				buffer += "male, ";
			else
				buffer += "male";
			
			okCounter--;
		}
		
		if(femaleOK) {
			if(okCounter > 1)
				buffer += "female, ";
			else
				buffer += "female";
		
			okCounter--;
		}
		
		if(otherOK) {
			if(okCounter > 1)
				buffer += "other, ";
			else
				buffer += "other";
			
			okCounter--;
		}
		
		return buffer;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_student_profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
